Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Sydent
Upstream-Contact: Richard van der Hoff <richard@matrix.org>
Source: https://github.com/matrix-org/sydent/

Files: .dockerignore
 .github/PULL_REQUEST_TEMPLATE.md
 .github/CODEOWNERS
 .github/workflows/changelog_check.yml
 .github/workflows/mypy.yml
 .github/workflows/pipeline.yml
 .gitignore
 CHANGELOG.md
 CONTRIBUTING.md
 Dockerfile
 MANIFEST.in
 README.rst
 changelog.d/.gitignore
 docs/casefold_migration.md
 docs/replication.md
 docs/templates.md
 matrix-sydent.service
 matrix_is_test/__init__.py
 matrix_is_test/res/is-test/invite_template.eml
 matrix_is_test/res/is-test/invite_template.eml.j2
 matrix_is_test/res/is-test/verification_template.eml
 matrix_is_test/res/is-test/verification_template.eml.j2
 matrix_is_test/res/is-test/verify_response_template.html
 matrix_is_test/terms.yaml
 pyproject.toml
 res/matrix-org/migration_template.eml.j2
 res/matrix-org/invite_template.eml
 res/matrix-org/invite_template.eml.j2
 res/matrix-org/verification_template.eml
 res/matrix-org/verification_template.eml.j2
 res/matrix-org/verify_response_template.html
 res/vector-im/invite_template.eml
 res/vector-im/invite_template.eml.j2
 res/vector-im/migration_template.eml.j2
 res/vector-im/verification_template.eml
 res/vector-im/verification_template.eml.j2
 res/vector-im/verify_response_template.html
 res/vector_verification_sample.txt
 scripts-dev/check_newsfragment.sh
 scripts-dev/lint.sh
 scripts/generate-key
 scripts/sydent-bind
 setup.cfg
 stubs/twisted/__init__.pyi
 stubs/twisted/internet/__init__.pyi
 stubs/twisted/internet/endpoints.pyi
 stubs/twisted/internet/error.pyi
 stubs/twisted/internet/ssl.pyi
 stubs/twisted/names/__init__.pyi
 stubs/twisted/names/dns.pyi
 stubs/twisted/python/__init__.pyi
 stubs/twisted/python/failure.pyi
 stubs/twisted/python/log.pyi
 stubs/twisted/web/__init__.pyi
 stubs/twisted/web/client.pyi
 stubs/twisted/web/http.pyi
 stubs/twisted/web/iweb.pyi
 stubs/twisted/web/resource.pyi
 stubs/twisted/web/server.pyi
 sydent/__init__.py
 sydent/db/__init__.py
 sydent/hs_federation/__init__.py
 sydent/http/__init__.py
 sydent/hs_federation/types.py
 sydent/sms/types.py
 sydent/util/versionstring.py
 sydent/sms/__init__.py
 sydent/replication/__init__.py
 sydent/terms/__init__.py
 sydent/users/__init__.py
 sydent/validators/common.py
 terms.sample.yaml
 tests/__init__.py
 tests/test_casefold_migration.py
 tests/test_invites.py
 tests/test_replication.py
 tests/test_start.py
 tests/test_util.py
 tests/utils.py
Copyright: 2014-2017, OpenMarket Ltd
  2017, Vector Creations Ltd
  2018-2019, New Vector Ltd
  2019-2021, The Matrix.org Foundation C.I.C.
License: Apache-2.0
Comment:
 Assuming copyright holders and license from other code files.

Files: matrix_is_test/launcher.py
 scripts/casefold_db.py
 sydent/config/__init__.py
 sydent/config/_base.py
 sydent/config/crypto.py
 sydent/config/database.py
 sydent/config/email.py
 sydent/config/general.py
 sydent/config/http.py
 sydent/config/sms.py
 sydent/db/accounts.py
 sydent/db/hashing_metadata.py
 sydent/db/terms.py
 sydent/http/auth.py
 sydent/http/blacklisting_reactor.py
 sydent/http/servlets/accountservlet.py
 sydent/http/servlets/hashdetailsservlet.py
 sydent/http/servlets/logoutservlet.py
 sydent/http/servlets/lookupv2servlet.py
 sydent/http/servlets/registerservlet.py
 sydent/http/servlets/termsservlet.py
 sydent/http/servlets/v2_servlet.py
 sydent/terms/terms.py
 sydent/types.py
 sydent/users/accounts.py
 sydent/users/tokens.py
 sydent/util/hash.py
 sydent/util/ip_range.py
 sydent/util/stringutils.py
 tests/test_auth.py
 tests/test_blacklisting.py
 tests/test_email.py
 tests/test_jinja_templates.py
 tests/test_msisdn.py
 tests/test_register.py
 tests/test_threepidunbind.py
Copyright: 2019, The Matrix.org Foundation C.I.C.
  2019-2021, The Matrix.org Foundation C.I.C.
  2020, The Matrix.org Foundation C.I.C.
  2021, The Matrix.org Foundation C.I.C.
License: Apache-2.0

Files: sydent/db/invite_tokens.py
 sydent/db/invite_tokens.sql
 sydent/db/peers.py
 sydent/db/peers.sql
 sydent/db/threepid_associations.py
 sydent/db/threepid_associations.sql
 sydent/db/threepid_validation.sql
 sydent/db/valsession.py
 sydent/http/httpclient.py
 sydent/http/httpcommon.py
 sydent/http/httpsclient.py
 sydent/http/servlets/__init__.py
 sydent/http/servlets/blindlysignstuffservlet.py
 sydent/http/servlets/bulklookupservlet.py
 sydent/http/servlets/emailservlet.py
 sydent/http/servlets/getvalidated3pidservlet.py
 sydent/http/servlets/pubkeyservlets.py
 sydent/http/servlets/replication.py
 sydent/http/servlets/store_invite_servlet.py
 sydent/sms/openmarket.py
 sydent/threepid/__init__.py
 sydent/threepid/signer.py
 sydent/util/__init__.py
 sydent/util/emailutils.py
 sydent/util/tokenutils.py
 sydent/validators/__init__.py
 sydent/validators/emailvalidator.py
Copyright: 2014, 2017, OpenMarket Ltd
  2014, OpenMarket Ltd
  2014-2015, OpenMarket Ltd
  2015, OpenMarket Ltd
  2016, OpenMarket Ltd
  2017, OpenMarket Ltd
License: Apache-2.0

Files: sydent/hs_federation/verifier.py
 sydent/http/federation_tls_options.py
 sydent/http/matrixfederationagent.py
 sydent/http/servlets/authenticated_bind_threepid_servlet.py
 sydent/util/ttlcache.py
Copyright: 2018, New Vector Ltd
  2019, New Vector Ltd
License: Apache-2.0

Files: sydent/db/sqlitedb.py
 sydent/http/servlets/threepidunbindservlet.py
 sydent/http/srvresolver.py
 sydent/replication/peer.py
 sydent/threepid/bind.py
Copyright: 2014, OpenMarket Ltd
  2014-2016, OpenMarket Ltd
  2018, New Vector Ltd
  2019, New Vector Ltd
License: Apache-2.0

Files: sydent/http/servlets/lookupservlet.py
 sydent/http/servlets/threepidbindservlet.py
 sydent/replication/pusher.py
Copyright: 2014, 2017, OpenMarket Ltd
  2014, OpenMarket Ltd
  2019, The Matrix.org Foundation C.I.C.
License: Apache-2.0

Files: sydent/config/exceptions.py
 tests/test_store_invite.py
Copyright: 2021, Matrix.org Foundation C.I.C.
License: Apache-2.0

Files: sydent/http/httpserver.py
 sydent/sydent.py
Copyright: 2014, OpenMarket Ltd
  2018, New Vector Ltd
  2019, The Matrix.org Foundation C.I.C.
License: Apache-2.0

Files: sydent/http/servlets/msisdnservlet.py
 sydent/validators/msisdnvalidator.py
Copyright: 2016, OpenMarket Ltd
  2017, Vector Creations Ltd
License: Apache-2.0

Files: sydent/http/servlets/authenticated_unbind_threepid_servlet.py
Copyright: 2020, Dirk Klimpel
License: Apache-2.0

Files: sydent/http/servlets/v1_servlet.py
Copyright: 2018, New Vector Ltd
  2018, Travis Ralston
License: Apache-2.0

Files: setup.py
Copyright: 2014, OpenMarket Ltd.
  2018, New Vector Ltd.
License: Apache-2.0

Files: debian/*
Copyright: 2021, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
      http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache 2.0 License
 can be found in /usr/share/common-licenses/Apache-2.0 file.
